function pn = perc_update(p, x, c, lam)
    x_tmp = [x; ones(1, size(x, 2))];
    for k = 1:size(x,2)
        res = sign(dot(x_tmp(:,k),p)) > 0;
        desired = c(1,k);
        error = desired - res;
        p(1,:) = p(1,:) + lam*error*x_tmp(:,k)';
    end
    pn = p;
end