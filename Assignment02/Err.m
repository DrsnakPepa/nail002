function E = Err(Name, NameL, Par, Tr, DTr, Ts, DTs)
    % Train the algorithm
    LPar = feval(Name, Tr, DTr, Par);
    
    % f is the number of wrongly classified patterns
    f = 0;
    
    for i = 1:size(Ts, 2)
        % Classify a pattern and increment f it was classified wrongly
        if feval(NameL, LPar, Ts(:,i)) ~= DTs(:,i)
           f = f + 1; 
        end
    end
    
    % Divide the number of wrongly classified patterns by the size of the testing data set
    E = f/size(Ts, 2);
end