function pn = perc_learn(p, x, c, lam, maxit)
    epoch = 1;
    error = perc_error(p, x, c);
    while epoch <= maxit && error > 0
        p = perc_update(p, x, c, lam);
        error = perc_error(p, x, c);
        epoch = epoch + 1;
    end
    pn = p;
end