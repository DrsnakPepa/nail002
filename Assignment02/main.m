% part A
In1 = csvread('In1.csv');
c1 = csvread('c1.csv');
Par1 = {[1 1 -1], 1, 10};
Par2 = {[1 1 -1], 1, 100};
[d, s] = CrossVal('PLearn', 'PRecall', Par1, 'PLearn', 'PRecall', Par2, In1, c1, 5, 'NoShuffle');
[d - tinv((95+100)/200, 4)*s, d + tinv((95+100)/200, 4)*s]

% part B
In2 = csvread('In2.csv');
c2 = csvread('c2.csv');
stream = RandStream.getGlobalStream; 
reset(stream);

Par3 = {rand(1, 5), 1, 50};

[d, s] = CrossVal('PLearn', 'PRecall', Par3, 'Memorizer', 'MemorizerRecall', {}, In2, c2, 6, 'NoShuffle');
[d - tinv((95+100)/200, 4)*s, d + tinv((95+100)/200, 4)*s]