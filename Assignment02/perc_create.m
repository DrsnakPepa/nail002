function p = perc_create(n)
    p = [rand(1, n) 0];
end