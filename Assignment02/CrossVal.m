function [delta, s] = CrossVal(Name1, Name1L, Par1, Name2, Name2L, Par2, Pat, DOut, k, NoShuffle)
    patLen = size(Pat, 2);
    
    % Generate indexes from 1 to number of patterns, randomly permuted if there is no NoShuffle
    indexes = 1:patLen;
    if nargin <= 9
        indexes = randperm(patLen);
    end
    
    % Preallocate k folds and their outputs
    F = {zeros(k)};
    D = {zeros(k)};
    
    % Split patterns into k equally long vectors using indexes vector,
    % which makes them randomly shuffled if shuffling is desired
    for i = 1:k
       F{i} = Pat(:, indexes(((patLen/k)*(i-1)+1):patLen/k*i));
       D{i} = DOut(:, indexes(((patLen/k)*(i-1)+1):patLen/k*i));
    end
    
    delta_vec = zeros(1, k);
    for i = 1:k
        % Indexes of test sets (all sets except for i-th)
        T_indexes = [1:i-1, i+1:k];
        
        % Create a vector of test data and desired outputs out of the cell array of folds 
        T = [F{T_indexes}];
        DT = [D{T_indexes}];
        
        % Update the delta using the output of the error function
        delta_vec(i) = Err(Name1, Name1L, Par1, F{i}, D{i}, T, DT) - Err(Name2, Name2L, Par2, F{i}, D{i}, T, DT);
    end
    
    % Calculate the delta
    delta = sum(delta_vec)/k;
    
    % Calculate the standard deviation
    s = sqrt(sum((delta_vec - delta).^2)/(k*(k-1)));
end