function S = Score(StudentResults, MaxPoints, PointsWeight)
    % set default MaxPoints and PointsWeight if not set
    if nargin == 1
        MaxPoints = 75;
        PointsWeight = 35;
    end
    
	% check input arguments
	if nargin ~= 1 && nargin ~= 3
		error('Usage: Score(StudentResults), Score(StudentResults, MaxPoints, PointsWeight)');
	end

	if MaxPoints <= 0
		error('MaxPoints must be positive!');
	end

	if PointsWeight <= 0 || PointsWeight > 100
		error('0 < PointsWeight <= 100');
	end

	% allocate needed vectors so their siuteze doesn't have to change every loop
	[sem, tests, final] = deal(zeros(size(StudentResults, 2), 1));

	% loop through StudentResults and save desired values to appropriate vectors
	for i = 1:size(StudentResults, 2)
		% if a student got more points from seminars than MaxPoints, use MaxPoints instead
		sem(i) = min(sum(StudentResults{i}{1}), MaxPoints)/MaxPoints*PointsWeight;
		tests(i) = sum(StudentResults{i}{2});
		final(i) = StudentResults{i}{3};
	end

	% final score equals to sum of points from seminars, tests and final exam
	% the sum cannot exceed 100
	S = min(sem+tests+final, 100);
end