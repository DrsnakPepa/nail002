function G = Grade(Score, Boundaries)
    % set default Boundaries if not set
    if nargin == 1
        Boundaries = [86, 71, 56];
    end
    
	% check input arguments
	if nargin > 2
		error('Usage: Grade(Score), Grade(Score, Boundaries)');
    end
    
	if Boundaries > 100 | Boundaries < 0
		error('0 <= Boundaries <= 100');
	end

	% create a binary matrix A with A[i,j] = 1 if score[i] < boundaries[j], 0 otherwise
	A = Score < Boundaries;

	% final grade equals to one (best grade possible) plus number of 1s in given row of A, which implies how many boundaries hasn't the score exceeded
	G = 1 + sum(A, 2);
end