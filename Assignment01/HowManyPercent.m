function P = HowManyPercent(TargetGrade, StudentResults, Boundaries, MaxPoints, PointsWeight, MaxExamScore)
    % set default values if not set
    if nargin == 2
        Boundaries = [86, 71, 56];
        MaxPoints = 75;
        PointsWeight = 35;
        MaxExamScore = 45;
    end
        
    % check input arguments
	if nargin ~= 2 && nargin ~= 6
		error('Usage: HowManyPercent(TargetGrade,StudentResults), HowManyPercent(TargetGrade,StudentResults,Boundaries,MaxPoints,PointsWeight,MaxExamScore)');
	end

	if size(StudentResults, 2) ~= size(TargetGrade, 1)
		error('TargetGrade and StudentResults must be of same size!');
	end

	if Boundaries > 100 | Boundaries < 0
		error('0 <= Boundaries <= 100');
	end

	if MaxPoints <= 0
		error('MaxPoints must be positive!');
	end

	if PointsWeight <= 0 || PointsWeight > 100
		error('0 < PointsWeight <= 100');
	end

	if MaxExamScore < 0
		error('MaxExamScore must be positive!');
	end

	% allocate needed vectors so their size doesn't have to change every loop
	[sem, tests ] = deal(zeros(size(StudentResults, 2), 1));

	% loop through StudentResults and save desired values to appropriate vectors
	for i = 1:size(StudentResults, 2)
		% if a student got more points from seminars than MaxPoints, use MaxPoints instead
		sem(i) = min(sum(StudentResults{i}{1}), MaxPoints)/MaxPoints*PointsWeight;
		tests(i) = sum(StudentResults{i}{2});
	end

	% add a 0 boundary for students aiming for the worst grade
	Boundaries = [Boundaries 0];

	% calculate score needed from final exam, negative results (already achieved grade) are converted to 0
	perc = max(Boundaries(TargetGrade)' - sem - tests, 0);

	% here, I am trying to avoid C-like if statement by forcing a 0/0 to generate NaN
	% could not come up with anything nicer, sorry
	P = perc./(perc <= MaxExamScore).*(perc <= MaxExamScore);
end